# Nginx + SSL Dockerfile

This is a Nginx Docker-Image with a self signed certificate. Every inbound will be redirected to the https url.

## Env

| Name         | Default | Description                                                                                                                                              |
| ------------ | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DH_SIZE      | 2048    | the Diffie-Hellman prime size in bytes. It can be used to determine how much memory must be allocated for the shared secret computed by DH_compute_key() |
